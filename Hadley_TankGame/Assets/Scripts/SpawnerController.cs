﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    public GameObject[] PowerUpType;
    public float spawnDelay;
    private float nextSpawnTime;
    private Transform tf;
    public UnityEngine.GameObject spawnedPickup;
 

    // Use this for initialization
    void Start ()
    {
        tf = gameObject.GetComponent<Transform>();
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Update is called once per frame
    void Update()
    {
        //only spawn if nothing
        if (spawnedPickup == null)
        {
            // can i spawn
            if (Time.time > nextSpawnTime)
            {

                // Spawn set time
                spawnedPickup = Instantiate(RandomPowerUp(), tf.position, Quaternion.identity) as UnityEngine.GameObject;
                nextSpawnTime = Time.time + spawnDelay;
            }
        } else
        {
            //objest still here delay
            nextSpawnTime = Time.time + spawnDelay;
        }
    }
    public GameObject RandomPowerUp()
    {
        return PowerUpType[UnityEngine.Random.Range(0, PowerUpType.Length)];
    }
}
