﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour {

    public int damage;
    public float destroytimer = 1f;
    public GameObject shooter;
    public PlayerController PC;
    public PlayerSFX SFX;

    private void Start()
    {
        SFX = gameObject.GetComponent<PlayerSFX>();
        if (shooter.GetComponent<PlayerController>() != null)
        {
            PC = shooter.GetComponent<PlayerController>();
        }
        //destroying game object affter set abount of time
        Destroy(gameObject, destroytimer);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (SFX != null)
        {
            SFX.Gettinghit();
        }
        if (collision.gameObject.GetComponent<TankData>() != null)
        {
            
            collision.gameObject.GetComponent<TankData>().Updatehealth(damage);

            if (PC != null && PC.lives >=1)
            {
                
                PC.Score += damage;

                Destroy(gameObject,1);
            }
        }
        
    }
    
}
