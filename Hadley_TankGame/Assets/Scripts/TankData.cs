﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {
    //variable for movespeed in meters per second
    public float moveSpeed = 1;
    //variable for turnspeed in degrees per second
    public float turnSpeed = 1;
    //bullet speed
    public float bulletSpeed;
    //buller damage
    public int bulletdamge;
    //delay for bullet timer
    public float bulletdelay = 1.0f;
    public float destroyAfterSeconds;
    //tank heath
    public float health;
    //tank max heath
    public float maxHealth;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
		
	}

    public void Updatehealth (int damage)
    {
        health -= damage;
    }
}
