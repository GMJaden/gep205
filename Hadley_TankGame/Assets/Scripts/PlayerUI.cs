﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

    public Text lives;
    public Image tank;
    
    public PlayerController player;

    public Text Score;
    private int numLives;
    private int numScore;

    // Use this for initialization
    void Start () {
        player = gameObject.GetComponentInParent<PlayerController>();
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        numScore = player.Score;
        numLives = player.lives;
        lives.text = ": " + numLives;
        Score.text = "Score: " + numScore;
	}
}
