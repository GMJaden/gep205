﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController: MonoBehaviour
{
    public TankData data;
    public List<Powerup> powerups;

    // Use this for initialization
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        powerups = new List<Powerup>();
    }

    // Update is called once per frame
    void Update()
    {
        //list to hold expierd power ups
        List<Powerup> OldPowerups = new List<Powerup>();

        //go throu list 
        foreach(Powerup power in powerups)
        {
            //remove time
            power.duration -= Time.deltaTime;

            //make list of old powers
            if(power.duration <=0)
            {
                OldPowerups.Add(power);
            }
        }

        //use list of old powr ups to remave powerups
        foreach(Powerup power in OldPowerups)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }
        //dont need to but lets clear list anway
        OldPowerups.Clear();

    }
    public void Add(Powerup powerUp)
    {
       
        powerUp.OnActivate(data);
        powerups.Add(powerUp);
    }
}