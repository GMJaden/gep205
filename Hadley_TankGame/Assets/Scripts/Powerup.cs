﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup
{
    public float speedMod;
    public float healthMod;
    public float MaxHealthMod;
    public float fireRateMod;
    public float damageMod;

    public float duration;

    public bool permanent;

    public void OnActivate(TankData target)
    {
        target.moveSpeed += speedMod;
        target.health += healthMod;
        target.maxHealth += MaxHealthMod;
        target.bulletdelay += fireRateMod;
    }
    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedMod;
        target.health -= healthMod;
        target.maxHealth -= MaxHealthMod;
        target.bulletdelay -= fireRateMod;
    }

}
