﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseFlee : MonoBehaviour
{
    public enum AttackMode { Chase, Flee };
    public AttackMode attackMode;
    public Transform target;
    public TankData data;
    public TankMotor motor;
    public float fleeDistance = 1.0f;
    public Transform tf;
    public float closeEnough = 1.0f;
    private float distance;

    // Use this for initialization
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(tf.position, target.position);
        if (attackMode == AttackMode.Chase)
        {

            //if to far away
            if (distance >= closeEnough)
            {
                
                // Rotate towards the target
                motor.RotateTowards(target.position, data.turnSpeed);
                // Move forward
                motor.Move(data.moveSpeed);
              
            }
            else
            {
                
                motor.Move(0);
            }
            
        }

        if (attackMode == AttackMode.Flee)
        {
            
            // distance to target
            Vector3 vectorToTarget = target.position - tf.position;
          
            // change direction to target
            Vector3 vectorAwayFromTarget = -1 * vectorToTarget;
            
            // normalize it
            vectorAwayFromTarget.Normalize();
       
            // ajusting distance from the target
            vectorAwayFromTarget *= fleeDistance;
        
            // getting the potion and moving to it
            Vector3 fleePosition = vectorAwayFromTarget + tf.position;
            //cheaking if to far away
            if(distance >= fleeDistance)
            {
                motor.Move(0);
            }
            else
            {
                motor.RotateTowards(fleePosition, data.turnSpeed);
                motor.Move(data.moveSpeed);
            }
            
        }
    }
}
