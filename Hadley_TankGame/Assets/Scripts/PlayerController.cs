﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    
    public TankData data;
    public TankMotor motor;
    public Shooting shooter;
    public PlayerSFX SFX;
    public int damage;
    public string objTag;
    public int Score;
    public int lives;
    private bool player1;
    private bool player2;

    public Camera SingleCam;
    public Camera multiCam;

    public enum InputScheme { WASD, arrowKeys };
    public InputScheme input = InputScheme.WASD;

    public GameManager gm;

   



    // Use this for initialization
    void Start ()
    {
        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<TankMotor>();
        shooter = gameObject.GetComponent<Shooting>();
        SFX = gameObject.GetComponent<PlayerSFX>();
        damage = data.bulletdamge;
        objTag = gameObject.tag;
        player1 = true;
        player2 = true;
        if (objTag == "Player")
        {
            if (GameManager.isSinglePlayer)
            {
                SingleCam.enabled = true;
                multiCam.enabled = false;
            }
            else if (GameManager.isMultiPlayer)
            {
                multiCam.enabled = true;
                SingleCam.enabled = false;

            }
        }
        else if (objTag =="Player2")
        {
           
            multiCam.enabled = true;
            SingleCam.enabled = false;
        }
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        
        if (lives >= 1)
        {
            switch (input)
            {
                case InputScheme.arrowKeys:
                    if (Input.GetKey(KeyCode.UpArrow))
                    {
                        motor.Move(data.moveSpeed);
                    }
                    if (Input.GetKey(KeyCode.DownArrow))
                    {
                        motor.Move(-data.moveSpeed);
                    }
                    if (Input.GetKey(KeyCode.RightArrow))
                    {
                        motor.Rotate(data.turnSpeed);
                    }
                    if (Input.GetKey(KeyCode.LeftArrow))
                    {
                        motor.Rotate(-data.turnSpeed);
                    }
                    if (Input.GetKey(KeyCode.RightControl))
                    {
                        SFX.TankShot();
                        shooter.Fire(damage);
                    }
                    break;

                case InputScheme.WASD:
                    if (Input.GetKey("w"))
                    {
                        motor.Move(data.moveSpeed);
                    }

                    if (Input.GetKey("s"))
                    {
                        motor.Move(data.moveSpeed * -1);
                    }
                    if (Input.GetKey("d"))
                    {
                        motor.Rotate(data.turnSpeed);
                    }
                    if (Input.GetKey("a"))
                    {
                        motor.Rotate(data.turnSpeed * -1);
                    }
                    // setting input for shooting
                    if (Input.GetButtonDown("Fire1"))
                    {
                        SFX.TankShot();
                        shooter.Fire(damage);

                    }
                    break;
            }
        }

            if (lives <= 0)
            {
            SFX.Death();

                if (objTag == "Player" && player1 == true )
                {
                    GameManager.player1Score = Score;
                    GameManager.player1Died = true;
                    player1 = false;
                }
                if (objTag == "Player2" && player2 == true)
                {
                    GameManager.player2Score = Score;
                    GameManager.player2Died = true;
                    player2 = false;
                    
                    
                }
            }
            else if (data.health <= 0)
            {
                lives--;
                data.health = data.maxHealth;
            }
        
        
    }
}
