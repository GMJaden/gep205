﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Rigidbody bullet;
    public UnityEngine.GameObject bulletSpawn;
    public TankData data;
    private float nextEventTime;
    public float destroytimer;
    public GameObject shooter;
    


    // Use this for initialization
    void Start ()
    {
        
        data = gameObject.GetComponent<TankData>();
        shooter = gameObject;
        nextEventTime = Time.time + data.bulletdelay;
        //seting bullet destruction
        destroytimer = data.destroyAfterSeconds;
       
        
    }
	
	//creating shooting function
    public void Fire(int D)
    {
        bullet.GetComponent<BulletDamage>().damage = D;
        bullet.GetComponent<BulletDamage>().shooter = shooter;
        if (nextEventTime <= Time.time)
        {
            // variable for rigiidbody
            Rigidbody clone;
            // creating clone and fiering
            clone = Instantiate(bullet, bulletSpawn.transform.position, transform.rotation) as Rigidbody;
            //adding force to bullet
            clone.AddForce(transform.forward * data.bulletSpeed);
            // rotating projetile to face the right way
            Quaternion q = Quaternion.FromToRotation(Vector3.up, transform.forward);
            clone.transform.rotation = q * clone.transform.rotation;
            //resetting timer for weapon delay
            nextEventTime = Time.time + data.bulletdelay;
        }
    }
}
