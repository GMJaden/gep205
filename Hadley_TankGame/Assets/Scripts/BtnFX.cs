﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BtnFX : MonoBehaviour {

    public AudioSource myFX;
    public AudioClip hoverFX;
    public AudioClip ClickFX;

    public void HoverSound()
    {
        myFX.PlayOneShot(hoverFX);
    }
    public void ClickSound()
    {
        myFX.PlayOneShot(ClickFX);
    }
}
