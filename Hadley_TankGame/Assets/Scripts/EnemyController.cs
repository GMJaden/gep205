﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public AIState aiState;
    public Personality pState;
    public int damage;
    public Transform target;
    public float avoidanceTime = 2.0f;
    public float closeEnough = 1.0f;
    public float fleeDistance = 1.0f;

    public enum AIState
    {
        Chase,
        CheckForFlee,
        Flee,
        Rest,
        TurnFire,
        Patrol
    };

    public enum Personality
    {
        SleepyTank,
        Agressivetank,
        FlankTank,
        TurretTank
    };

    public float stateEnterTime;
    public float aiSenseRadius;
    public float restingHealRate;

    public enum LoopType
    {
        Loop,
        PingPong
    };

    public LoopType loopType;
    public float fieldOfView = 100;
    public float hightMultiplyer = 1.0f;
    public float sightDistance = 1.0f;

    public Transform[] players;
    public Transform[] waypoints;
    public GameObject spawnPoint;
    public GameObject player;
    public int numWayPoints;
    private int howManyWaypoints;



    private GameManager gm;
    private int avoidanceStage = 0;
    private Transform tf;
    private float exitTime;
    private float distance;
    private TankData data;
    private TankMotor motor;
    private Shooting shooter;
    private int currentpoint = 0;
    private bool patrolForward = true;



    // Use this for initialization
    void Start()
    {

        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<TankMotor>();
        shooter = gameObject.GetComponent<Shooting>();
        damage = this.data.bulletdamge;
        tf = gameObject.GetComponent<Transform>();
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }

        //setting how many whay points the npc will go to
        waypoints = new Transform[numWayPoints];
        //getting how many there are in total
        howManyWaypoints = GameManager.instance.waypointsObj.Length;
        //running function the see what whay point the npc will go to
        GetWaypointsLocation();


    }

    // Update is called once per frame
    void Update()
    {
        //getting closest enemy
        ClosestEnemy();
        //gettting distance to target
        distance = (target.position - tf.position).magnitude;




        //setting personality of the tank
        switch (pState)
        {
            case Personality.SleepyTank:
                SFight();
                break;
            case Personality.FlankTank:
                FFight();
                break;
            case Personality.TurretTank:
                TTank();
                break;
            case Personality.Agressivetank:
                AFight();
                break;

        }

        if (data.health <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void AFight()
    {
        //geting the state of the tank
        if (aiState == AIState.Patrol)
        {

            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoPatrol();
            }

            if (distance < aiSenseRadius)
            {
                if (target.GetComponentInParent<PlayerController>().lives >= 1)
                {
                    //change state based on distance
                    ChangeState(AIState.Chase);
                }
            }


        }

        if (aiState == AIState.Chase)
        {

            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
                //if can see shooting
                Sight();

            }


        }
    }

    public void SFight()
    {
        //getting the Ai state
        if (aiState == AIState.Patrol)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoPatrol();
            }

            if (distance < aiSenseRadius)
            {
                if (target.GetComponentInParent<PlayerController>().lives >= 1)
                {
                    //change state based on distance
                    ChangeState(AIState.Chase);
                }
            }

            if (data.health < data.maxHealth * 0.5f)
            {
                //change stat based on health
                ChangeState(AIState.CheckForFlee);
            }
        }

        if (aiState == AIState.Chase)
        {
            //check heath
            if (data.health < data.maxHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }

            // checking avoidance
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
                //if can see shooting
                if(target.GetComponentInParent<PlayerController>().lives >= 1)
                Sight();
            }
        }
        else if (aiState == AIState.Flee)
        {
            // cheack if in avoidance
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {

                DoFlee();
            }

            // Check for how long in state
            if (Time.time >= stateEnterTime + 5)
            {
                //change state based on time
                ChangeState(AIState.CheckForFlee);
            }
        }
        else if (aiState == AIState.CheckForFlee)
        {

            CheckForFlee();

        }
        else if (aiState == AIState.Rest)
        {
            // Perform Behaviors
            DoRest();

            // Check for Transitions
            if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
            {
                //changeingstat based on distance
                ChangeState(AIState.Flee);
            }
            else if (data.health >= data.maxHealth)
            {
                //changeing state based on health
                ChangeState(AIState.Chase);
            }
        }
    }

    public void FFight()
    {
        // setting offset
        float offset = 5;
        //point poting to the right of target
        Vector3 flank = target.right * offset;
        //get distance to flank location
        float flankdistance = (target.position + flank - tf.position).magnitude;
        //checking personality state
        if (aiState == AIState.Patrol)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoPatrol();
            }

            if (distance < aiSenseRadius)
            {

                if (target.GetComponentInParent<PlayerController>().lives >= 1)
                {
                    //change state based on distance
                    ChangeState(AIState.Chase);
                }
            }
        }

        if (aiState == AIState.Chase)
        {

            if (avoidanceStage != 0)
            {

                DoAvoidance();
            }
        }


        else
        {

            // looking and movent to flank potion
            motor.RotateTowards(target.position + flank, data.turnSpeed);
            motor.Move(data.moveSpeed);
        }

        if (distance < closeEnough)
        {

            //when in potion look and shoot target   
            ChangeState(AIState.TurnFire);
        }


        if (aiState == AIState.TurnFire)
        {
            if (target.GetComponentInParent<PlayerController>().lives >= 1)
            {
                DOTurnFire();
                if (distance > closeEnough)
                {
                    //change state based on distance
                    ChangeState(AIState.Chase);
                }
            }

        }
    }


    public void TTank()
    {
        //cheaking ai state
        if (aiState == AIState.Patrol)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoPatrol();
            }

            if (distance < aiSenseRadius)
            {
                DOTurnFire();
                if (distance > closeEnough)
                {
                    //changing state if close enough
                    ChangeState(AIState.TurnFire);
                }



            }

            if (aiState == AIState.TurnFire)
            {
                //look and shoot at target
                DOTurnFire();
                if (distance >= aiSenseRadius)
                {
                    ChangeState(AIState.Patrol);
                }

            }
        }

    }

    public void DoPatrol()
    {
        //rotating to way points
        motor.RotateTowards(waypoints[currentpoint].position, data.turnSpeed);
        if (CanMove(data.moveSpeed))
        {

            //if can move moove to waypooints
            motor.Move(data.moveSpeed);

        }
        else
        {
            avoidanceStage = 1;
        }

        if (Vector3.SqrMagnitude(waypoints[currentpoint].position - tf.position) < (closeEnough * closeEnough))
        {
            if (loopType == LoopType.Loop)
            {

                //move to nest waypoint
                if (currentpoint < waypoints.Length - 1)
                {
                    currentpoint++;
                }
                else
                {
                    currentpoint = 0;
                }
            }

            else if (loopType == LoopType.PingPong)
            {
                if (patrolForward)
                {

                    //nest way point
                    if (currentpoint < waypoints.Length - 1)
                    {
                        currentpoint++;
                    }
                    else
                    {
                        //reverse direction then subrat point
                        patrolForward = false;
                        currentpoint--;
                    }
                }
                else
                {
                    //nest waypoint
                    if (currentpoint > 0)
                    {
                        currentpoint--;
                    }
                    else
                    {
                        //reverse and incrment way point
                        patrolForward = true;
                        currentpoint++;
                    }
                }

            }

        }
    }


    public void DOTurnFire()
    {

        //stop tank
        motor.Move(0);

        //look at player
        motor.RotateTowards(target.position, data.turnSpeed);

        //shoot
        Sight();

    }

    public void CheckForFlee()
    {
        // Check for range
        if (distance <= aiSenseRadius)
        {
            ChangeState(AIState.Flee);
        }
        else
        {
            ChangeState(AIState.Rest);
        }
    }

    public void DoRest()
    {
        // regen heath
        data.health += restingHealRate * Time.deltaTime;

        // dont go over max heath
        data.health = Mathf.Min(data.health, data.maxHealth);
    }

    public void ChangeState(AIState newState)
    {
        // Change  state
        aiState = newState;

        // time we changed states
        stateEnterTime = Time.time;
    }

    public void DoChase()
    {

        //rotat to player
        motor.RotateTowards(target.position, data.turnSpeed);
        //check distance
        if (distance >= closeEnough)
        {

            //if can moon
            if (CanMove(data.moveSpeed))
            {

                //drive forward
                motor.Move(data.moveSpeed);
            }
            else
            {

                //set avoidance to one
                avoidanceStage = 1;
            }
        }
        else
        {

            motor.Move(0);
        }
    }

    public void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // Rotate left
            motor.Rotate(-5 * data.turnSpeed);

            // cheack if we can move forward
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                // how long we will stay in avoidance
                exitTime = avoidanceTime;
            }
        }
        else if (avoidanceStage == 2)
        {
            // cheack if we can move forward
            if (CanMove(data.moveSpeed))
            {
                // countdown and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                // when timer done chase player
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }

            else
            {
                // if we cand do others go to avoidance
                avoidanceStage = 1;
            }
        }
    }

    bool CanMove(float speed)
    {

        //cast a ray
        RaycastHit hit;
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {

            //compare tags for player
            if (!hit.collider.CompareTag("Player"))
            {

                return false;
            }
        }

        return true;
    }

    public void DoFlee()
    {


        // distance to target
        Vector3 vectorToTarget = target.position - tf.position;

        // revers direction
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        // normalize it
        vectorAwayFromTarget.Normalize();

        // addding distance from target
        vectorAwayFromTarget *= fleeDistance;

        // getting the potion and moving to it
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.turnSpeed);
        motor.Move(data.moveSpeed);


    }

    void Sight()
    {
        //casting ray
        RaycastHit hit;
        if (Physics.Raycast(tf.position + Vector3.up * hightMultiplyer, tf.forward, out hit, sightDistance))
        {
            //if in sight and its player shoot
            if (hit.collider.gameObject.tag == "Player" || hit.collider.gameObject.tag == "Player2")
            {
                shooter.Fire(damage);
            }
        }
    }

    void GetWaypointsLocation()
    {
        //loopint trhou each waypoint
        for (int i = 0; i < waypoints.Length; i++)
        {
            //picking waypoints on the map at random
            waypoints[i] = GameManager.instance.waypointsObj[Random.Range(0, howManyWaypoints)].transform;
        }
    }

    void ClosestEnemy()
    {
        float distancetoclosestplayer = Mathf.Infinity;
        PlayerController closest = null;
        PlayerController[] allplayer = GameObject.FindObjectsOfType<PlayerController>();
        foreach (PlayerController currentPlayer in allplayer)
        {
            float distancetoplayer = (currentPlayer.transform.position - this.transform.position).sqrMagnitude;
            if (distancetoplayer < distancetoclosestplayer)
            {
                distancetoclosestplayer = distancetoplayer;
                closest = currentPlayer;
            }
        }

        if (closest.lives >= 1)
        {
            target = closest.transform;
        }
    }
};

