﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSFX : MonoBehaviour {

    public AudioSource TankShoot;
    public AudioSource TankDeath;
    public AudioSource BullitHit;
    public AudioSource pickup;


    public void TankShot ()
    {
        TankShoot.Play();
    }
    public void Death()
    {
        TankDeath.Play();
    }

    public void Gettinghit()
    {
        BullitHit.Play();
    }

    public void PowerUp()
    {
        pickup.Play();
    }
}
