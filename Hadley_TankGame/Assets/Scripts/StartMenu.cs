﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    
    public void SinglePlayer()
    {
        GameManager.isSinglePlayer = true;
        GameManager.isMultiPlayer = false;
        GameManager.player2Died = true;
        GameManager.player1Died = false;
    }
    public void MultiPlayer()
    {
        GameManager.isSinglePlayer = false;
        GameManager.isMultiPlayer = true;
    }
    public void RandomMap()
    {
        GameManager.randomMap = true;
        GameManager.mapOfDay = false;
        SceneManager.LoadScene(1);
    }
    public void MapOfTheDay()
    {
        GameManager.randomMap = false;
        GameManager.mapOfDay = true;
        SceneManager.LoadScene(1);
    }

    public void ResetGame ()
    {
        GameManager.isSinglePlayer = false;
        GameManager.isMultiPlayer = false;

        GameManager.player1Died = false;
        GameManager.player2Died = false;


        GameManager.randomMap = false;
        GameManager.mapOfDay = false;
        SceneManager.LoadScene(0);
    }

    public void QuitNow()
    {
        Application.Quit();
    }
    

}
