﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PpwerSpawner : MonoBehaviour {

    public GameObject[] PowerUpType;
    public bool PowerUpSpawned;
    public GameObject SpawnedPowerUp;
    public GameObject thisSpawner;
    

	// Use this for initialization
	void Start () {
        SpawnedPowerUp = RandomPowerUp();
        Instantiate(SpawnedPowerUp, gameObject.transform.position, gameObject.transform.rotation);
        thisSpawner = gameObject;
        PowerUpSpawned = true;
       
    }
	
	// Update is called once per frame
	void Update () {

		if(SpawnedPowerUp.GetComponent<Pickup>().powerupGone == true)
        {
            Instantiate(SpawnedPowerUp, gameObject.transform.position, gameObject.transform.rotation);
        }
	}
    public GameObject RandomPowerUp()
    {
        return PowerUpType[UnityEngine.Random.Range(0, PowerUpType.Length)];
    }
}
