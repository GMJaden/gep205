﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour {

    //variable for charactercontroller
    private CharacterController cc;
    
    //storing the transfor for the gameobject so i dont have to call all the time
    public Transform tf;

	// Use this for initialization
	void Start () {

        //geting charatercontroler component
        cc = gameObject.GetComponent<CharacterController>();
        //getting transofom component
        tf = gameObject.GetComponent<Transform>();
	}

    public void Move(float speed)
    {
        //vector for holding speed data
        Vector3 speedVector;

        //geting vector diraction andd adding speed
        speedVector = tf.forward * speed;

        //call simple move and give vector
        cc.SimpleMove(speedVector);

    }

    public void Rotate (float speed)
    {
        // storing vector data
        Vector3 rotateVector;

        //getting vecot data and anding speed and time to it
        rotateVector = Vector3.up * speed * Time.deltaTime;

        //Rotate tank in locl space not world space and apping rotatin data
        tf.Rotate(rotateVector, Space.Self);
    }

    // can we rotate towards target
    public bool RotateTowards(Vector3 target, float speed) 
    {
        
        //vetor3 variable 
        Vector3 vectorToTarget;
        //getting distance to target
        vectorToTarget = target - tf.position;
        //getting Quaternion

        if (target.z <= 0)
        {
           
            Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
            //checking if looking at target
            if (targetRotation == tf.rotation)
            {
               
                return false;
            }
            else
            {
               
                tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);
                return true;
            }
        }
        else
        {
            
            return false;
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
