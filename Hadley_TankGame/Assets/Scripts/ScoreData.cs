﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

    [System.Serializable]
    public class ScoreData : IComparable<ScoreData>
    {
        public float score;
        public string name;

    public ScoreData (string newName, float newScore)
    {
        name = newName;
        score = newScore;
    }

        public int CompareTo(ScoreData other)
        {
            if (other == null)
            {
                return 1;
            }
            if (this.score > other.score)
            {
                return 1;
            }
            if (this.score < other.score)
            {
                return -1;
            }
            return 0;
        }
    }
