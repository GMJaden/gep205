﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Transform tf;
    public Powerup powerup;
    public PlayerSFX SFX;
    public bool SFXplayed;
    public bool powerupGone;
    public GameObject spawner;

    // Use this for initialization
    void Start () {
        SFX = gameObject.GetComponent<PlayerSFX>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnTriggerEnter(Collider other)
    {

        
        //variable foe controller
        PowerUpController powCon = other.GetComponent<PowerUpController>();

        //if has controller
        if (powCon != null)
        {
            SFX.PowerUp();
            SFXplayed = true;
            //Add
            powCon.Add(powerup);
            powerupGone = true;
            

            //destroy
            Destroy(gameObject,1);
        }
    }
}
