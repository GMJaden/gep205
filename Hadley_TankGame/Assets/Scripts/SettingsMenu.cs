﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour {

    public AudioMixer musicVolume;
    public AudioMixer sfxVolume;
    public float MVolume;
    public float FXVolume;

    public Slider musicSlider;
    public Slider SFXSlider;

    public void Start()
    {
        MVolume = PlayerPrefs.GetFloat("MusicVolume",0);
        FXVolume = PlayerPrefs.GetFloat("SFXVolume");
        musicVolume.SetFloat("MusicVolume", MVolume);
        sfxVolume.SetFloat("SFXVolume", FXVolume);
        musicSlider.value = MVolume;
        SFXSlider.value = FXVolume;
    }



    public void SetVolume (float volume)
    {
        musicVolume.SetFloat("MusicVolume", volume);
        MVolume = volume;
       
    }

    public void SetSFX(float volume)
    {
        sfxVolume.SetFloat("SFXVolume", volume);
        FXVolume = volume;
        
    }
    public void Save()
    {
        PlayerPrefs.SetFloat("MusicVolume", MVolume);
        PlayerPrefs.SetFloat("SFXVolume", FXVolume);
    }
   

}
