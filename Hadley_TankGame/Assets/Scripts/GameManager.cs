﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager: MonoBehaviour {

    public static GameManager instance;
    public UnityEngine.GameObject[] enemys;
    public UnityEngine.GameObject[] powerUps;
    public UnityEngine.GameObject[] waypointsObj;
    public UnityEngine.GameObject[] playerSpawn;
    public GameObject player1;
    public GameObject player2;
    public UnityEngine.GameObject[] Enemytypes;
    public static bool player1Died;
    public static bool player2Died;
    public static int player1Score;
    public static int player2Score;


    public int rows;
    public int cols;
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;
    public UnityEngine.GameObject[] gridPrefabs;
    private Room[,] grid;

    public int mapSeed;
    public static bool mapOfDay;
    public static bool randomMap;
    public static bool isSinglePlayer;
    public static bool isMultiPlayer;

    public int endspawn;

    public List<ScoreData> scoresList;
    
    // Runs before any Start() functions run
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("ERROR: There can only be one GameManager.");
            Destroy(gameObject);
        }

    }

    // Use this for initialization
    void Start()
    {
        //cheaking bools
        if (randomMap)
        {
            //Settings seed to cutent time
            mapSeed = DateInt(DateTime.Now);
        }
        else if (mapOfDay)
        {
            //setting seed to day
            mapSeed = DateInt(DateTime.Now.Date);
        }
        if (isSinglePlayer)
        {
            //seed
            UnityEngine.Random.InitState(mapSeed);

            // Generate Grid
            GenerateGrid();

            waypointsObj = UnityEngine.GameObject.FindGameObjectsWithTag("Waypoint");
            playerSpawn = UnityEngine.GameObject.FindGameObjectsWithTag("PlayerSpawn");

            //placing fighters
            PlaceFighters();
        }
        else if(isMultiPlayer)
        {
            //seed
            UnityEngine.Random.InitState(mapSeed);

            // Generate Grid
            GenerateGrid();

            waypointsObj = UnityEngine.GameObject.FindGameObjectsWithTag("Waypoint");
            playerSpawn = UnityEngine.GameObject.FindGameObjectsWithTag("PlayerSpawn");
            int endspawn = playerSpawn.Length;

            //placing fighters
            PlaceFighters();
        }
        else
        {
            //do nothing
        }
        scoresList = new List<ScoreData>();


    }

    // Update is called once per frame
    void Update ()
    {
        enemys = UnityEngine.GameObject.FindGameObjectsWithTag("Enemy");
        powerUps = UnityEngine.GameObject.FindGameObjectsWithTag("PowerUp");
        if (player1Died&&player2Died)
        {

            scoresList.Add(new ScoreData(PlayerPrefs.GetString("HighScoreName"), PlayerPrefs.GetFloat("HighScore")));
            scoresList.Add(new ScoreData(player1.name, player1Score));
           scoresList.Add(new ScoreData(player2.name, player2Score));
           scoresList.Sort();
           scoresList.Reverse();
            scoresList.GetRange(0, 3);
            PlayerPrefs.SetFloat("HighScore",scoresList[0].score);
            PlayerPrefs.SetString("HighScoreName", scoresList[0].name);

            SceneManager.LoadScene(2);
        }
    }
    public void GenerateGrid()
    {
        //clear out the grid
        grid = new Room[rows, cols];

        //for each grid row...
        for (int RowNum = 0; RowNum < rows; RowNum++)
        {
            //for each column 
            for (int ColumNum = 0; ColumNum < cols; ColumNum++)
            {
                //get  location. 
                float xPosition = roomWidth * ColumNum;
                float zPosition = roomHeight * RowNum;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                // Create a new grid at location
                UnityEngine.GameObject tempRoomObj = Instantiate((UnityEngine.GameObject)RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;

                //set its parent
                tempRoomObj.transform.parent = this.transform;

                //name it
                tempRoomObj.name = "Room_" + ColumNum + "," + RowNum;

                //get the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                //open doors
                //bottom row open north door
                if (RowNum == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (RowNum == rows - 1)
                {
                    //top row open south door
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    //middle open both doors
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                //first column open the east door
                if (ColumNum == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (ColumNum == cols - 1)
                {
                    //last column row open west door
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    //middle open both doors
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }
                //save grid array
                grid[RowNum,ColumNum] = tempRoom;
            }
        }
    }

    //random room
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    public int DateInt(DateTime dateToUse)
    {
       
            //addd date up
            return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
        
    }

   public void PlaceFighters()
    {
        if (GameManager.isSinglePlayer)
        {
            //placing the player
            Instantiate(player1, playerSpawn[0].transform.position, playerSpawn[0].transform.rotation);
            for (int i = 1; i < waypointsObj.Length; i++)
            {
                Instantiate((UnityEngine.GameObject)RandomEnemys(), waypointsObj[i].transform.position, waypointsObj[i].transform.rotation);
            }
        }
        else if(GameManager.isMultiPlayer)
        {
            int endspawn = playerSpawn.Length;
            Instantiate(player1, playerSpawn[0].transform.position, playerSpawn[0].transform.rotation);
            Instantiate(player2,playerSpawn[UnityEngine.Random.Range(0, playerSpawn.Length)].transform.position, playerSpawn[UnityEngine.Random.Range(0, playerSpawn.Length)].transform.rotation);
            for (int i = 1; i < waypointsObj.Length; i++)
            {
                Instantiate((UnityEngine.GameObject)RandomEnemys(), waypointsObj[i].transform.position, waypointsObj[i].transform.rotation);
            }
        }
    }

    public GameObject RandomEnemys()
    {
        return Enemytypes[UnityEngine.Random.Range(0,Enemytypes.Length)];
    }

}
