﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGeneratorScript : MonoBehaviour
{
    public int rows;
    public int cols;
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;
    public UnityEngine.GameObject[] gridPrefabs;
    private Room[,] grid;


    // Use this for initialization
    void Start()
    {
        // Generate Grid
        GenerateGrid();
    }

    public void GenerateGrid()
    {
        //clear out the grid
        grid = new Room[rows, cols];

        //for each grid row...
        for (int RowNum = 0; RowNum < rows; RowNum++)
        {
            //for each column 
            for (int ColumNum = 0; ColumNum < cols; ColumNum++)
            {
                //get  location. 
                float xPosition = roomWidth * ColumNum;
                float zPosition = roomHeight * RowNum;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                // Create a new grid at location
                UnityEngine.GameObject tempRoomObj = Instantiate((UnityEngine.GameObject)RandomRoomPrefab(), newPosition, Quaternion.identity) as UnityEngine.GameObject;

                //set its parent
                tempRoomObj.transform.parent = this.transform;

                //name it
                tempRoomObj.name = "Room_" + ColumNum + "," + RowNum;

                //get the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                //open doors
                //bottom row open north door
                if (RowNum == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (RowNum == rows - 1)
                {
                    //top row open south door
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    //middle open both doors
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                //first column open the east door
                if (ColumNum == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (ColumNum == cols - 1)
                {
                    //last column row open west door
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    //middle open both doors
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }
                //save grid array
                grid[ColumNum, RowNum] = tempRoom;
            }
        }
    }

    //random room
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[Random.Range(0, gridPrefabs.Length)];
    }
}
